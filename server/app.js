const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const config = require('config');
const orderApi = require('./routes/orderRoute');
const { bot, tgSendMessage } = require('./helpers/telegraf/telefraf');

const PORT = process.env.PORT || config.get('PORT');
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api', orderApi);

bot.launch();
app.listen(PORT, () => {
    try {
        console.log("\nServer is started on ", PORT, " port. \nHappy hacking!\n");
    } catch (e) {
        console.log(e.message);
        tgSendMessage(config.get("tgAdminChatID"), e.message);
    }
});

