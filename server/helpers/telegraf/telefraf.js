const { Telegraf } = require('telegraf');
const config = require('config');

const bot = new Telegraf(config.get('tgToken'));

bot.start(ctx => {
   ctx.reply('Бот запущен.');
});

const tgSendMessage = (chatID, message) => {
    try {
        bot.telegram.sendMessage(chatID, message);
    } catch (e) {
        bot.telegram.sendMessage(chatID, e.message);
    }
};

module.exports = {bot, tgSendMessage};
