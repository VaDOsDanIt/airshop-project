const jwt = require('jsonwebtoken');
const config = require('config');

const generateToken = (user) => {
    const data = {
        name: user.name,
        surname: user.surname,
        tel: user.tel
    };
    const secret = config.get('jwtSecret');
    const expiration = config.get('jswExpires');

    return jwt.sign(data, secret, {expiresIn: expiration});
};

const verifyToken = (token) => {
    return jwt.verify(token, config.get('jwtSecret'), (err, decoded) => err === null);
};

module.exports = {generateToken, verifyToken};