const express = require('express');
const config = require('config');
const {tgSendMessage} = require('../helpers/telegraf/telefraf');
const {verifyToken, generateToken} = require('../helpers/jwt/jwt');

const router = express.Router();

router.post('/order', async (req, res) => {
    try {
        const {token, name, surname, tel, region, city, post} = req.body;
        if (verifyToken(token) !== true) {
            const newToken = generateToken({name, surname, tel, region, city, post});
            await tgSendMessage(config.get('tgAdminChatID'), `
Имя: ${name}
Фамилия: ${surname}
Номер телефона: ${tel}
Область: ${region}
Город: ${city}
Отделение новой почты: №${post}`);
            res.send({message: "Запрос принят", token: newToken});
        } else {
            res.send({message: "Вы уже отправляли запрос, попробуйте отправить запрос позже."});
        }
    } catch (e) {
        res.send({error: e.message, message: "Произошла ошибка. Попробуйте сделать это позже!"});
        tgSendMessage(config.get("tgAdminChatID"), e.message);
        console.log(e.message);
    }
});

module.exports = router;