const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const cssmin = require('gulp-cssmin');
const rigger = require('gulp-rigger');
const image = require('gulp-image');
const browserSync = require('browser-sync').create();

gulp.task('html', () => {
    return gulp.src('src/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('dist/'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('sass', () => {
    return gulp.src('src/scss/**/style.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cssmin())
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('image', () => {
    return gulp.src('src/img/**/*')
        .pipe(image())
        .pipe(gulp.dest('dist/img'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('js', () => {
    return gulp.src('src/js/**/*.js')
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.reload({
            stream: true
        }));
});


gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: "dist/"
        }
    });

    gulp.watch("src/scss/**/*.scss", gulp.parallel('sass'));
    gulp.watch("src/**/*.html", gulp.parallel('html'));
    gulp.watch("src/js/**/*.js", gulp.parallel('js'));
    gulp.watch("src/img/**/*", gulp.parallel('image'));
});


gulp.task('default', gulp.parallel('html', 'sass', 'image', 'js'));
gulp.task('dev', gulp.parallel('html', 'sass', 'image', 'js', 'serve'));