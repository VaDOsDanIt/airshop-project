$('.header-checkout-btn').click(() => {
    $("html, body").animate({scrollTop: $("header").height() + 6000}, "slow");
});

$(".input-tel-js").mask("+38(099)999-99-99");

$(document).ready(() => {
    $('.checkout-form').validate({
        errorPlacement: (error, element) => {
            error.insertBefore(element);
        },
        onsubmit: false,
        rules: {
            name: {
                required: true,
                number: false
            },
            surname: {
                required: true,
                number: false
            },
            tel: {
                required: true,
            },
            region: {
                required: true,
            },
            city: {
                required: true,
            },
            post: {
                required: true,
            }
        },
        messages: {
            name: {
                required: "Введите имя."
            },
            surname: {
                required: "Введите фамилию."
            },
            tel: {
                required: "Введите номер телефона."
            },
            region: {
                required: "Введите область."
            },
            city: {
                required: "Введите город."
            },
            post: {
                required: "Введите отделение новой почты."
            }
        },
    });


   $('.send-order-btn').click(e => {
       e.preventDefault();
       if($('.checkout-form').valid()){
           let obj = {};
           $('.checkout-input').each((i, item) => {
               const name = $(item).data('label');
               const value = item.value;
               Object.assign(obj, {[name]: value});
           });

           Object.assign(obj, {token: localStorage.getItem('token') || null});

           $.ajax({
               type: "POST",
               url: "https://airshop.net.ua/api/order/",
               data: obj,
               success: (e) => {
                   if(e.token !== undefined) localStorage.setItem("token", e.token);
                   const eMessageElement = $('.checkout-form-label .e-message-js');
                   eMessageElement.text('');
                   eMessageElement.append(e.message);
               }
           })
       }
   })

});

